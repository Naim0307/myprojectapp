﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IndexUI.aspx.cs" Inherits="UserDefineTypeWeb.IndexUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>
        <asp:TextBox ID="firstNameTextBox" runat="server" style="margin-left: 48px" Width="230px"></asp:TextBox>
        <br/>
        <br/>
        <asp:Label ID="Label2" runat="server" Text="Middle Name"></asp:Label>
        <asp:TextBox ID="middleNameTextBox" runat="server" style="margin-left: 23px" Width="234px"></asp:TextBox>
        <br/>
        <br/>
        <asp:Label ID="Label3" runat="server" Text="Last Name"></asp:Label>
        <asp:TextBox ID="lastNameTextBox" runat="server" style="margin-left: 44px" Width="234px"></asp:TextBox>
        <br/>
        <br/>
        <asp:Button ID="savedButton" runat="server" Text="Saved" OnClick="savedButton_Click" />
        <br/>
        <br/>
        <asp:Button ID="viewAllFullNameButton" runat="server" Text="View All Full Name" OnClick="viewAllFullNameButton_Click" />
        <br/>
        <br/>
        <asp:ListBox ID="fullNameListBox" runat="server" Height="244px" Width="575px"></asp:ListBox>
        <br/>
        <br/>
    </div>
    </form>
</body>
</html>
