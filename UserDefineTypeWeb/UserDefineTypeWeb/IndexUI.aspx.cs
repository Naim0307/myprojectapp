﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UserDefineTypeWeb
{
    public partial class IndexUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void savedButton_Click(object sender, EventArgs e)
        {
            // Person aPerson = new Person();   use only saved a person
            List<Person> persons;
            Person aPerson = new Person();
            aPerson.firstName = firstNameTextBox.Text;
            aPerson.middleName = middleNameTextBox.Text;
            aPerson.lastName = lastNameTextBox.Text;
            firstNameTextBox.Text = String.Empty;
            middleNameTextBox.Text = String.Empty;
            lastNameTextBox.Text = String.Empty;
            //ViewState["Person"] = aPerson; use only saved a person
            if (ViewState["Persons"] == null)
            {
                persons = new List<Person>();
            }
            else
            {
                persons = (List<Person>)ViewState["Persons"];
            }
            persons.Add(aPerson);
            ViewState["Persons"] = persons;
        }

        protected void viewAllFullNameButton_Click(object sender, EventArgs e)
        {
            //Person aPerson =(Person)ViewState["Person"];     use only saved a person
            //string fullName = aPerson.firstName + " " + aPerson.middleName + " " + aPerson.lastName; use only saved a person
            fullNameListBox.Items.Clear();
            List<Person> persons = (List<Person>)ViewState["Persons"];
            foreach (Person aPerson in persons)
            {
                //string fullName = aPerson.firstName + " " + aPerson.middleName + " " + aPerson.lastName;
                fullNameListBox.Items.Add(aPerson.GetFullName());
            }
            //fullNameListBox.Items.Add(fullName);

        }
    }
}