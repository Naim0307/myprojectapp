﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;

namespace UserDefineTypeWeb
{
    [Serializable] //if we save a class in view state we must use Serializable in this class
    public class Person
    {
        public string firstName;
        public string middleName;
        public string lastName;

        public string GetFullName()
        {
            string fullname = firstName + " " + middleName + " " + lastName;
            return fullname;
        }
    }
}