﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalaryCalculateWebApplication
{
    [Serializable]
    public class Salary
    {
        public double basicSalary;
        public double medicalAllowance;
        public double conveyanceAllowance;

        public double GetSalary()
        {
            double medicalAmount = basicSalary * (medicalAllowance / 100);
            double conveyanceAmount = basicSalary * (conveyanceAllowance / 100);
            double totalSalary = basicSalary + medicalAllowance + conveyanceAllowance;
            return totalSalary;
        }
    }
}