﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SalaryCalculateWebApplication
{
    public partial class IndexUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void saveSalaryButton_Click(object sender, EventArgs e)
        {
            Salary aSalary = new Salary();
            aSalary.basicSalary = Convert.ToDouble(basicSalaryTextBox.Text);
            aSalary.conveyanceAllowance = Convert.ToDouble(conveyanceAllowanceTextBox.Text);
            aSalary.medicalAllowance = Convert.ToDouble(medicalAllowanceTextBox.Text);

            basicSalaryTextBox.Text = String.Empty;
            conveyanceAllowanceTextBox.Text = String.Empty;
            medicalAllowanceTextBox.Text = String.Empty;
            ViewState["Salary"] = aSalary;
            //salaryLabel.Text = aSalary.GetSalary() + "Tk";
        }

        protected void showSalaryButton_Click(object sender, EventArgs e)
        {
            Salary aSalary = (Salary)ViewState["Salary"];
            //salaryLabel.Text = Convert.ToString(aSalary.GetSalary());
            salaryLabel.Text = aSalary.GetSalary() + "Tk";
        }
    }
}