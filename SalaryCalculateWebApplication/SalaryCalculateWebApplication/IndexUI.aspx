﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IndexUI.aspx.cs" Inherits="SalaryCalculateWebApplication.IndexUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
        <tr>
            <th> Basic Salary : </th>
            <td><asp:TextBox ID="basicSalaryTextBox" runat="server"></asp:TextBox> </td>
        </tr>
        <tr>
            <th> Medical Allowance : </th>
            <td><asp:TextBox ID="medicalAllowanceTextBox" runat="server"></asp:TextBox> </td>
            <td> % of Basic</td>
        </tr>
        <tr>
            <th> Conveyance Allowance: </th>
            <td><asp:TextBox ID="conveyanceAllowanceTextBox" runat="server"></asp:TextBox> </td>
            <td> % of Basic</td>
        </tr>
        <tr>
            <td><asp:Button ID="saveSalaryButton" runat="server" Text="Saved Salary" OnClick="saveSalaryButton_Click" /></td>
        </tr>
        <tr>
            <td><asp:Button ID="showSalaryButton" runat="server" Text="Show Salary" OnClick="showSalaryButton_Click" /></td>
        </tr>
        <tr>
            <td> 
                <asp:Label ID="salaryLabel" runat="server" Text="*********"></asp:Label>

            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
