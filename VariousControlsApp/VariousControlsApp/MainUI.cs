﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VariousControlsApp
{
    public partial class MainUI : Form
    {
        public MainUI()
        {
            InitializeComponent();
        }

        private void mealButton_Click(object sender, EventArgs e)
        {

            string items = mealComboBox.Text;
            MessageBox.Show(items, "Food Item-Meal", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            MessageBox.Show("No Items Are Selected");

        }

        private void dessertButtton_Click(object sender, EventArgs e)
        {
            if (iceCreamCheckBox.Checked)
            {
                MessageBox.Show(iceCreamCheckBox.Text);
            }
            else if(falludaCheckBox.Checked)
            {
                MessageBox.Show(falludaCheckBox.Text);
            }
            else if(mixedFruitCheckBox.Checked)
            {
                MessageBox.Show(mixedFruitCheckBox.Text);
            }
            else if (puddingCheckBox.Checked)
            {
                MessageBox.Show(puddingCheckBox.Text);
            }
            else
            {
                MessageBox.Show("No Items Are Selectd");
            }
        }

        private void showBeverageButton_Click(object sender, EventArgs e)
        {
            if (cookeRadioButton.Checked)
            {
                MessageBox.Show(cookeRadioButton.Text);
            }
            else if (fruitDrinkRadioButton.Checked)
            {
                MessageBox.Show(fruitDrinkRadioButton.Text);
            }
            else if(coffeRadioButton.Checked)
            {
                MessageBox.Show(coffeRadioButton.Text);
            }
            else if(teaRadioButton.Checked)
            {
                MessageBox.Show(teaRadioButton.Text);
            }
            else
            {
                MessageBox.Show("No Items Are Selectd");
            }
        }

    
    }
}
