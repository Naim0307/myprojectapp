﻿namespace VariousControlsApp
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mealComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mealButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dessertButtton = new System.Windows.Forms.Button();
            this.iceCreamCheckBox = new System.Windows.Forms.CheckBox();
            this.falludaCheckBox = new System.Windows.Forms.CheckBox();
            this.puddingCheckBox = new System.Windows.Forms.CheckBox();
            this.mixedFruitCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.showBeverageButton = new System.Windows.Forms.Button();
            this.cookeRadioButton = new System.Windows.Forms.RadioButton();
            this.fruitDrinkRadioButton = new System.Windows.Forms.RadioButton();
            this.teaRadioButton = new System.Windows.Forms.RadioButton();
            this.coffeRadioButton = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // mealComboBox
            // 
            this.mealComboBox.FormattingEnabled = true;
            this.mealComboBox.Items.AddRange(new object[] {
            "Haydarbadi Mattoun Biriyani",
            "Chiken Tandur",
            "Passta",
            "Panta Illish"});
            this.mealComboBox.Location = new System.Drawing.Point(166, 47);
            this.mealComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mealComboBox.Name = "mealComboBox";
            this.mealComboBox.Size = new System.Drawing.Size(364, 30);
            this.mealComboBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Food Items";
            // 
            // mealButton
            // 
            this.mealButton.BackColor = System.Drawing.Color.SpringGreen;
            this.mealButton.Location = new System.Drawing.Point(681, 47);
            this.mealButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mealButton.Name = "mealButton";
            this.mealButton.Size = new System.Drawing.Size(103, 32);
            this.mealButton.TabIndex = 2;
            this.mealButton.Text = "Show";
            this.mealButton.UseVisualStyleBackColor = false;
            this.mealButton.Click += new System.EventHandler(this.mealButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.groupBox1.AutoSize = true;
            this.groupBox1.BackColor = System.Drawing.Color.SeaGreen;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.mealButton);
            this.groupBox1.Controls.Add(this.mealComboBox);
            this.groupBox1.Location = new System.Drawing.Point(81, 34);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(1016, 142);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Meals Items";
            // 
            // groupBox2
            // 
            this.groupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.groupBox2.AutoSize = true;
            this.groupBox2.BackColor = System.Drawing.Color.SeaGreen;
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBox2.Controls.Add(this.mixedFruitCheckBox);
            this.groupBox2.Controls.Add(this.puddingCheckBox);
            this.groupBox2.Controls.Add(this.falludaCheckBox);
            this.groupBox2.Controls.Add(this.iceCreamCheckBox);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.dessertButtton);
            this.groupBox2.Location = new System.Drawing.Point(81, 193);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1016, 376);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dessert Items";
            // 
            // dessertButtton
            // 
            this.dessertButtton.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.dessertButtton.Location = new System.Drawing.Point(681, 83);
            this.dessertButtton.Margin = new System.Windows.Forms.Padding(4);
            this.dessertButtton.Name = "dessertButtton";
            this.dessertButtton.Size = new System.Drawing.Size(103, 32);
            this.dessertButtton.TabIndex = 2;
            this.dessertButtton.Text = "Show";
            this.dessertButtton.UseVisualStyleBackColor = false;
            this.dessertButtton.Click += new System.EventHandler(this.dessertButtton_Click);
            // 
            // iceCreamCheckBox
            // 
            this.iceCreamCheckBox.AutoSize = true;
            this.iceCreamCheckBox.BackColor = System.Drawing.Color.SeaGreen;
            this.iceCreamCheckBox.Location = new System.Drawing.Point(53, 57);
            this.iceCreamCheckBox.Name = "iceCreamCheckBox";
            this.iceCreamCheckBox.Size = new System.Drawing.Size(127, 28);
            this.iceCreamCheckBox.TabIndex = 3;
            this.iceCreamCheckBox.Text = "Ice Cream";
            this.iceCreamCheckBox.UseVisualStyleBackColor = false;
            // 
            // falludaCheckBox
            // 
            this.falludaCheckBox.AutoSize = true;
            this.falludaCheckBox.BackColor = System.Drawing.Color.SeaGreen;
            this.falludaCheckBox.Location = new System.Drawing.Point(441, 57);
            this.falludaCheckBox.Name = "falludaCheckBox";
            this.falludaCheckBox.Size = new System.Drawing.Size(101, 28);
            this.falludaCheckBox.TabIndex = 3;
            this.falludaCheckBox.Text = "Falluda";
            this.falludaCheckBox.UseVisualStyleBackColor = false;
            // 
            // puddingCheckBox
            // 
            this.puddingCheckBox.AutoSize = true;
            this.puddingCheckBox.Location = new System.Drawing.Point(441, 120);
            this.puddingCheckBox.Name = "puddingCheckBox";
            this.puddingCheckBox.Size = new System.Drawing.Size(110, 28);
            this.puddingCheckBox.TabIndex = 3;
            this.puddingCheckBox.Text = "Pudding";
            this.puddingCheckBox.UseVisualStyleBackColor = true;
            // 
            // mixedFruitCheckBox
            // 
            this.mixedFruitCheckBox.AutoSize = true;
            this.mixedFruitCheckBox.BackColor = System.Drawing.Color.SeaGreen;
            this.mixedFruitCheckBox.Location = new System.Drawing.Point(53, 120);
            this.mixedFruitCheckBox.Name = "mixedFruitCheckBox";
            this.mixedFruitCheckBox.Size = new System.Drawing.Size(137, 28);
            this.mixedFruitCheckBox.TabIndex = 3;
            this.mixedFruitCheckBox.Text = "Mixed Fruit";
            this.mixedFruitCheckBox.UseVisualStyleBackColor = false;
            // 
            // groupBox3
            // 
            this.groupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.groupBox3.AutoSize = true;
            this.groupBox3.BackColor = System.Drawing.Color.SeaGreen;
            this.groupBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBox3.Controls.Add(this.coffeRadioButton);
            this.groupBox3.Controls.Add(this.teaRadioButton);
            this.groupBox3.Controls.Add(this.fruitDrinkRadioButton);
            this.groupBox3.Controls.Add(this.cookeRadioButton);
            this.groupBox3.Controls.Add(this.showBeverageButton);
            this.groupBox3.Location = new System.Drawing.Point(81, 401);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(1016, 196);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Beverage Items";
            // 
            // showBeverageButton
            // 
            this.showBeverageButton.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.showBeverageButton.Location = new System.Drawing.Point(681, 107);
            this.showBeverageButton.Margin = new System.Windows.Forms.Padding(4);
            this.showBeverageButton.Name = "showBeverageButton";
            this.showBeverageButton.Size = new System.Drawing.Size(103, 32);
            this.showBeverageButton.TabIndex = 2;
            this.showBeverageButton.Text = "Show";
            this.showBeverageButton.UseVisualStyleBackColor = false;
            this.showBeverageButton.Click += new System.EventHandler(this.showBeverageButton_Click);
            // 
            // cookeRadioButton
            // 
            this.cookeRadioButton.AutoSize = true;
            this.cookeRadioButton.Location = new System.Drawing.Point(53, 68);
            this.cookeRadioButton.Name = "cookeRadioButton";
            this.cookeRadioButton.Size = new System.Drawing.Size(91, 28);
            this.cookeRadioButton.TabIndex = 3;
            this.cookeRadioButton.TabStop = true;
            this.cookeRadioButton.Text = "Cooke";
            this.cookeRadioButton.UseVisualStyleBackColor = true;
            // 
            // fruitDrinkRadioButton
            // 
            this.fruitDrinkRadioButton.AutoSize = true;
            this.fruitDrinkRadioButton.Location = new System.Drawing.Point(430, 68);
            this.fruitDrinkRadioButton.Name = "fruitDrinkRadioButton";
            this.fruitDrinkRadioButton.Size = new System.Drawing.Size(127, 28);
            this.fruitDrinkRadioButton.TabIndex = 3;
            this.fruitDrinkRadioButton.TabStop = true;
            this.fruitDrinkRadioButton.Text = "Fruit Drink";
            this.fruitDrinkRadioButton.UseVisualStyleBackColor = true;
            // 
            // teaRadioButton
            // 
            this.teaRadioButton.AutoSize = true;
            this.teaRadioButton.Location = new System.Drawing.Point(430, 140);
            this.teaRadioButton.Name = "teaRadioButton";
            this.teaRadioButton.Size = new System.Drawing.Size(67, 28);
            this.teaRadioButton.TabIndex = 3;
            this.teaRadioButton.TabStop = true;
            this.teaRadioButton.Text = "Tea";
            this.teaRadioButton.UseVisualStyleBackColor = true;
            // 
            // coffeRadioButton
            // 
            this.coffeRadioButton.AutoSize = true;
            this.coffeRadioButton.Location = new System.Drawing.Point(53, 140);
            this.coffeRadioButton.Name = "coffeRadioButton";
            this.coffeRadioButton.Size = new System.Drawing.Size(79, 28);
            this.coffeRadioButton.TabIndex = 3;
            this.coffeRadioButton.TabStop = true;
            this.coffeRadioButton.Text = "Coffe";
            this.coffeRadioButton.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.button1.Location = new System.Drawing.Point(681, 315);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 32);
            this.button1.TabIndex = 2;
            this.button1.Text = "Show";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SpringGreen;
            this.ClientSize = new System.Drawing.Size(1196, 744);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainUI";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox mealComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button mealButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox mixedFruitCheckBox;
        private System.Windows.Forms.CheckBox puddingCheckBox;
        private System.Windows.Forms.CheckBox falludaCheckBox;
        private System.Windows.Forms.CheckBox iceCreamCheckBox;
        private System.Windows.Forms.Button dessertButtton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button showBeverageButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton coffeRadioButton;
        private System.Windows.Forms.RadioButton teaRadioButton;
        private System.Windows.Forms.RadioButton fruitDrinkRadioButton;
        private System.Windows.Forms.RadioButton cookeRadioButton;
    }
}

