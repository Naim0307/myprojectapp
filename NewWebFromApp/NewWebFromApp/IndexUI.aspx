﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IndexUI.aspx.cs" Inherits="NewWebFromApp.IndexUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Text="Name:"></asp:Label>
        <asp:TextBox ID="nameTextBox" runat="server"></asp:TextBox>
        <br/>
        <br/>
        <asp:Button ID="saveButton" runat="server" Text="Save" OnClick="saveButton_Click" />
        <br/>
        <br/>
        <asp:Button ID="showAllButton" runat="server" Text="Show All" OnClick="showAllButton_Click" />
        <br/>
        <br/>
        <asp:ListBox ID="namesListBox" runat="server" Height="228px" Width="430px"></asp:ListBox>
        <br/>
        <br/>
        <asp:Label ID="countLabel" runat="server" Text="********"></asp:Label>
    </div>
        
        
    </form>
</body>
</html>
