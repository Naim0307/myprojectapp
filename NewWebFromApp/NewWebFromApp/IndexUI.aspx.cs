﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NewWebFromApp
{
    public partial class IndexUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            string name = nameTextBox.Text;
            nameTextBox.Text = null;
            List<string> nameList;
            if (ViewState["Names"]== null)
            {
               nameList = new List<string>();
            }
            else
            {
                nameList = (List<string>)ViewState["Names"];
            }
            nameList.Add(name);
            ViewState["Names"] = nameList;
        }

        protected void showAllButton_Click(object sender, EventArgs e)
        {
            namesListBox.Items.Clear();
            List<string> extructNameList =(List<string>) ViewState["Names"];
            foreach (string name in extructNameList)
            {
                namesListBox.Items.Add(name);
            }

            countLabel.Text = namesListBox.Items.Count.ToString();
        }
    }
}