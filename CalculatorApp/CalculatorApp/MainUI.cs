﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorApp
{
    public partial class CalculatorUI : Form
    {
        public CalculatorUI()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            double firstnumber = Convert.ToDouble(firstNumberTextBox.Text);
            double secoundnumber = Convert.ToDouble(secoundNumberTextBox.Text);
            double result= Add (firstnumber, secoundnumber);
            resultTextBox.Text = Convert.ToString(result);
        }

        double Add(double firstNo,double secoundNo)
        {
            return firstNo + secoundNo;
        }

        private void substructButton_Click(object sender, EventArgs e)
        {
            double firstnumber = Convert.ToDouble(firstNumberTextBox.Text);
            double secoundnumber = Convert.ToDouble(secoundNumberTextBox.Text);
            double result = Substeuct(firstnumber, secoundnumber);
            resultTextBox.Text = Convert.ToString(result);
        }

        double Substeuct(double firstNo, double secoundNo)
        {
            return firstNo - secoundNo;
        }
        private void multiplyButton_Click(object sender, EventArgs e)
        {
            double firstnumber = Convert.ToDouble(firstNumberTextBox.Text);
            double secoudnumber = Convert.ToDouble(secoundNumberTextBox.Text);
            double result = Multiply(firstnumber, secoudnumber);
            resultTextBox.Text = Convert.ToString(result);
        }

        double Multiply(double firstNo, double secoundNo)
        {
            return firstNo * secoundNo;
        }

        private void divideButton_Click(object sender, EventArgs e)
        {
            double firstnumber = Convert.ToDouble(firstNumberTextBox.Text);
            double secoundnumber = Convert.ToDouble(secoundNumberTextBox.Text);
            double result = Devision(firstnumber, secoundnumber);
            resultTextBox.Text = Convert.ToString(result);
        }

        double Devision(double firstNo, double secoundNo)
        {
            return firstNo / secoundNo;
        }
    }
}
