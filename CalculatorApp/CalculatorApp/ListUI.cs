﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorApp
{
    public partial class ListUI : Form
    {
        public ListUI()
        {
            InitializeComponent();
        }

        private void showAllButton_Click(object sender, EventArgs e)
        {
            int number = Convert.ToInt32(numberTextBox.Text);
            outputListBox.Items.Clear();
            for (int i = 1; i <= number; i++)
            {
                outputListBox.Items.Add(i);
            }
        }
    }
}
